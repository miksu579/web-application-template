# Web application template

## Frontend

* React
* styled components
* axios calls

### Get frontend started
* npm i
* npm start

## Backend

* Node.js
* express server
* middleware functions for requestBody, CORS and requestDateTime prebuilt
* Sequelize integrated: C and partly R works from CRUD

### Get backend started
* npm install
* node app.js optionally nodemon app.js


- A web application template with React front end and Node.js express back end.
- Intent to create a template for simple CRUD applications