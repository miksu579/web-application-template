require("dotenv").config();
const express = require("express");
const app = express();
const port = process.env.SERVER_PORT;
const welcome = require("./routes/welcome");
const getItems = require("./routes/getItems");
const getCategories = require("./routes/getCategories");
const addItem = require("./routes/addItem");
const addCategory = require("./routes/addCategory");
const {_logRequestBody, _logRequestDateTime, _cors} = require("./services/middleware");
const {Sequelize} = require('sequelize');

//CORS handling
app.use(_cors);
app.use(express.json());
// Ability to do something on every time a request comes to backend
app.use(_logRequestDateTime);

app.use("/", welcome);
// [middleware._logRequestBody] grants ability to do something everytime before route goes to getItems
app.use("/getItems", [_logRequestBody], getItems);
app.use("/getCategories", [_logRequestBody], getCategories);

app.use("/addItem", [_logRequestBody], addItem);
app.use("/addCategory", [_logRequestBody], addCategory);

// app.use("/updateItem", [_logRequestBody], addCategory);



app.listen(port, () => {
  console.log(`template app listening at http://localhost:${port}`);
});
