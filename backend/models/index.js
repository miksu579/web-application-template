const { Sequelize, DataTypes } = require("sequelize");

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASS,
  {
    host: process.env.DB_HOST,
    dialect: "mysql",
  }
  );

  const Category = sequelize.define(
    "Categories",
    {
      // Model attributes are defined here
      Name: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    },
    {
      // Other model options go here
    }
  );
  
const Item = sequelize.define(
  "Items",
  {
    // Model attributes are defined here
    Name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    CategoryId:{
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  },
  {
    // Other model options go here
  }
);

Item.belongsTo(Category);


// `sequelize.define` also returns the model

// START XAMPP FROM TERMINAL ON MAC
//   sudo /Applications/XAMPP/xamppfiles/xampp stop 

// console.log(Category === sequelize.models.Category); // true
// console.log(Item === sequelize.models.Item); // true

// Category.sync({ force: true });
// Item.sync({ force: true });

module.exports = { Item, Category, sequelize };
