const express = require("express");
const addCategory = express.Router();
const { createCategory } = require("../services/dataBaseActions");

addCategory.post("/", async (req, res) => {
  let data = { ...req.body };
  data.Name = data.Name.toString()
  console.log(data);
  await createCategory(data.Name);
  res.sendStatus(200);
});

module.exports = addCategory;
