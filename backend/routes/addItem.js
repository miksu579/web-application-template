const express = require("express");
const addItem = express.Router();
const { createItem } = require("../services/dataBaseActions");

addItem.post("/", async (req, res) => {
  let data = { ...req.body };
  data.CategoryId = parseInt(data.CategoryId);
  data.Name = data.Name.toString()
  await createItem(data.Name, data.CategoryId);
  res.sendStatus(200);
});

module.exports = addItem;
