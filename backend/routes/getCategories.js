const express = require('express');
const getCategories = express.Router()
const {connection, getAllCategories} = require('../services/dataBaseActions');

getCategories.get('/',  async (req,res) =>{
    const allCategories = await getAllCategories();
    // modifies allWords list to contain only the words before returning it to frontend
    const categoryList = [];
    allCategories.forEach(category => {
        categoryList.push(category.dataValues);
    });
    // console.log(categoryList);
    res.send(categoryList);
})

module.exports = getCategories;