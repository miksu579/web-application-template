const express = require('express');
const getItems = express.Router()
const {connection, getAllItems} = require('../services/dataBaseActions');

getItems.get('/',  async (req,res) =>{
    const allItems = await getAllItems();
    // modifies allItems list to contain only the Items before returning it to frontend
    const ItemList = [];
    allItems.forEach(item => {
        ItemList.push(item.dataValues);
    });
    console.log(ItemList);
    res.send(ItemList);
})

module.exports = getItems;