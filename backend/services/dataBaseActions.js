require('dotenv').config()
const {Item, Category} = require('../models/index');
const mysql = require('mysql');

const connection = mysql.createConnection({
    host : process.env.DB_HOST,
    user : process.env.DB_USER,
    password : process.env.DB_PASS,
    database : process.env.DB_NAME,
});

// For database actions, you want to define what attributes you want to fetch from database e.g. ["Name","Id"]

const createItem = async (name, categoryId) =>{
    // This part explodes if there isn't a category premade for categoryId
    const newItem = await Item.create({Name:name, CategoryId:categoryId}, {fields: ["Name", "CategoryId"]});
    console.log(newItem.id);
}

const createCategory = async (name) =>{
    const newCategory = await Category.create({Name:name}, {fields: ["Name"]});
    console.log(newCategory.id);
}

const getAllItems = async () =>{
    const allItems = await Item.findAll({attributes:["Name", "Id", "CategoryId", "createdAt", "updatedAt"]});
    return allItems;
}

const getAllCategories = async () =>{
    const allCategories = await Category.findAll({attributes: ["Name", "Id"]});
    return allCategories;
}



module.exports =  {connection, createItem, getAllItems, getAllCategories, createCategory}