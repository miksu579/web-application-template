const _logRequestBody = (req, res, next) => {
  console.log(req.body);
  next();
};

const _logRequestDateTime = (req, res, next) => {
  console.log("request time: ",Date.now());
  next();
};

const _cors = (req,res,next) =>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Auhtorization"
    );
    if (req.method === "OPTIONS") {
      res.header("Access-Control-Allow-Methods", "POST, GET");
      return res.status(200).json({});
    }
    next();
}

module.exports = { _logRequestBody, _logRequestDateTime, _cors };
