import React, { useState, useEffect } from "react";
import styled from "styled-components";
import getAllDataFromTable from "./routes/getAllDataFromTable";
import addItem from "./routes/addItem";
import addCategory from "./routes/addCategory";
import ItemList from "./components/list/ItemList";
import Search from "./components/search/Search";
import DynamicalForm from "./components/dynamicalForm/DynamicalForm";

const Button = styled.button`
  padding: 5px 10px;
  margin: 40px;
`;

const DataDiv = styled.div``;

const App = () => {
  const [items, setItems] = useState([]);
  const [filteredItems, setFilteredItems] = useState([]);
  const [categories, setCategories] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [selectedUnit, setSelectedUnit] = useState({});
  const [selectedUnitAsArray, setSelectedUnitAsArray] = useState([]);

  useEffect(async () => {
    const currentItems = await getAllDataFromTable("getItems");
    const currentCategories = await getAllDataFromTable("getCategories");
    setCategories(currentCategories);
    let allUnits =[...currentCategories, ...currentItems]
    setItems(allUnits);
    setFilteredItems(allUnits);
  }, []);

  const selectUnit = (e) => {
    console.log(e.target.innerHTML);
    let currentSelectedUnit = {};
    items.forEach((item) => {
      if (parseInt(e.target.id) === parseInt(item.Id) && e.target.innerHTML === item.Name) {
        currentSelectedUnit = item;
      }
    });
    if (currentSelectedUnit !== {}) {
      selectedUnitToArray(currentSelectedUnit);
      setSelectedUnit(currentSelectedUnit);
      // console.log(currentSelectedUnit);
    }
  };

  const selectedUnitToArray = (unit) => {
    let UnitAsArray = [];
    Object.keys(unit).forEach((key) => {
      UnitAsArray.push({ name: key });
    });
    Object.values(unit).forEach((value, i) => {
      UnitAsArray[i].value = value;
    });
    setSelectedUnitAsArray(UnitAsArray);
    // console.log(UnitAsArray);
  };

  return (
    <div className="App">
      
      <Search
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        items={items}
        filteredItems={filteredItems}
        setFilteredItems={setFilteredItems}
        ></Search>
        <br/>
      <Button
        onClick={async () => {
          await addItem("xItem", "1");
          const updatedItems = await getAllDataFromTable("getItems");
          setItems(updatedItems);
          setFilteredItems(updatedItems);
          // console.log(items);
        }}
      >
        Send mock Item
      </Button>
      <Button
        onClick={async () => {
          await addCategory("xCategory");
          const updatedCategories = await getAllDataFromTable("getCategories");
          setCategories(updatedCategories);
          // console.log(categories);
        }}
      >
        Send mock category
      </Button>
      <ItemList
        class="items"
        listName="Items"
        data={filteredItems}
        selectUnit={selectUnit}
      ></ItemList>
      {/* <ItemList
        class="categories"
        listName="Categories"
        data={categories}
        selectUnit={selectUnit}
      ></ItemList>
      <br /> */}
      <DynamicalForm selectedUnit={selectedUnitAsArray}></DynamicalForm>

    </div>
  );
};

export default App;
