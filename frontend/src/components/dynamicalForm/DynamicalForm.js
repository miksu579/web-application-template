// This is a dynamical CRUD form for all kinds of applications.

import React, { Component, useState, useEffect } from "react";

const UpdateItemForm = (props) => {
  const [inputList, setInputListValue] = useState([{ name: "", value: "" }]);
  

  useEffect(async () => {
    setInputListValue(props.selectedUnit);
  });
  
  // make inputs controlled
  const handleInputChange = (e,i) => {
    const inputListCopy = [ ...inputList ];
    const {value} = e.target;
    inputListCopy[i].value = value;
    setInputListValue(inputListCopy);
  };

  const valdiateInput = (e) => {
    // Insert some validation like regex or something here. Front end validation isn't that important because the important part is that no invalid or hazardous input goes into db.
    if (e.target.value) {
      console.log("something happened");
    }
  };
  // Some ideas for future development:

  // form would be good if it would understand the difference between having an existing object, having a completely new type or a new object of some type.
  // basically three use cases:
  // 1. user wants to add a new object type
  // 2. user wants to add a new new object of certain type
  // 3. user wants to update an existing object

  // Should the form contain the understanding of the difference in actions(save, update...)?
  // Maybe it could have buttons for all the functionality available, but only relevant buttons are created.
  //  - how do you know which are relevant?
  // Could be just a dumb dynamical form, which shows the information it's asked

  // TODO to achieve this functionality:

  // - A form which satisfies the needs of all situations
  //    * Determination needed
  //      - for delete button to be rendered
  //      - creating the type name input and property inputs
  //    * Determination not needed
  //      - creating inputs from data like type attributes
  // 
  
  // 1. user wants to add a new object type
  //    - a form that has
  //      * name of new type
  //      * a field that has label of property 1 and a controlled input for it
  //        - has functionality,
  //          * when user types stuff into it, create next input for next property
  //          * if input is empty, when focusout -> delete input
  //          * inputs could have a placeholder text which says "if empty, will not be saved" - solves the last input being always empty
  //    - Save button
  //    - Cancel button

  // 2. user wants to add a new new object of certain type
  // - Create a dropdown from which the user can select the type of object
  //    * Create a separate array of type objects, which have all the required fields of that type
  //    * when user selects a type, form dynamically creates required fields for that type for user to fill
  // - Save button
  // - Cancel button

  //  3. user wants to update an existing object
  //    - User selects the object -> create and fill required fields
  //    - Save button
  //    - Cancel button
  //    - Delete button

  return (
    <form className="form-container">
      {inputList.map((input, i) => {
        return (
          <div className="input-container" key={i}>
            <label htmlFor={`input-${i}`}>{input.name}: </label>
            <input
              id={`input-${i}`}
              value={input.value}
              onChange={(e) => {
                handleInputChange(e,i);
                // valdiateInput(e);
              }}
            ></input>
          </div>
        );
      })}
    </form>
  );
};
export default UpdateItemForm;
