import React, {Component} from 'react';

const Form = () =>{
    const [inputValue, setInputValue] = useState("");

    // make input controlled
    const handleInputChange = (e) =>{
        setInputValue(e.target.value)
    }

    const valdiateInput = (e) =>{
        // Insert some validation like regex or something here. Front end validation isn't that important because the important part is that no invalid or hazardous input goes into db.
        if(e.target.value){
            console.log("something happened");
        }
    }

    return(
        <form className="form-container">
            {/* You can copy this container and get awesome inputs */}
            <div className="input-container">
                <label htmlFor="input1">Insert text: </label>
                <input id="input1" value={inputValue} onChange={(e) =>{
                    handleInputChange(e);
                    valdiateInput(e);
                }}></input>
            </div>
        </form>
    );
}
export default Form;
