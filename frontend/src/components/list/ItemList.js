import React from "react";
import Unit from '../unit/Unit';


const ItemList = (props) => {
  return (
    <div className="ItemList-container">
      <header>{props.listName}</header>
      {props.data.map((item, index) =>{
        return <Unit className="item" key={index} data={item} selectUnit={props.selectUnit}></Unit>
      })}
    </div>
  );
};

export default ItemList;
