import React from "react";

const Search = (props) => {
  // make input controlled
  const handleSearchChange = (e) => {
    const {value} = e.target;
    props.setSearchValue(value);
    const newFilteredItems = props.items.filter((item) => {
      if(item.Name.toLowerCase().includes(value.toLowerCase())){
        return true;
      }
    });

    props.setFilteredItems(newFilteredItems);

    // console.log(props.searchValue);
  };

  const valdiateInput = (e) => {
    // Insert some validation like regex or something here. Front end validation isn't that important because the important part is that no invalid or hazardous input goes into db.
    if (e.target.value) {
      // console.log("something happened");
    }
  };

  return (
    <div className="search-container">
      <label htmlFor="search-input">Search: </label>
      <input
        name="search"
        id="search-input"
        value={props.searchValue}
        onChange={(e) => {
          handleSearchChange(e);
          valdiateInput(e);
        }}
      ></input>
      {/* {console.log(props.filteredItems)} */}
    </div>
  );
};
export default Search;
