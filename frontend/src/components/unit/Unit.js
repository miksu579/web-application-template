import React from 'react';

const Unit = (props) =>{
    return (
    <div className="unit" id={props.data.Id} onClick={e => props.selectUnit(e)}>{props.data.Name}</div>
    );  
} 
export default Unit;