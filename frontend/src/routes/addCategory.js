import ncsAxios from "../utils/ncsAxios";

//generic adder for categories
const addCategory = async (name) => {
  const response = await ncsAxios.post(`/addCategory`, { Name: name });
  return response;
};

export default addCategory;
