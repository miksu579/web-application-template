import ncsAxios from "../utils/ncsAxios";

//generic adder for data
const addItem = async (name, categoryId) => {
  const response = await ncsAxios.post(`/addItem`, {
    Name: name,
    CategoryId: categoryId,
  });
  return response;
};

export default addItem;
