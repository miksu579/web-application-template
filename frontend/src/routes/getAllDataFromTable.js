import ncsAxios from "../utils/ncsAxios";

//generic getter for data
// available routes getItems, getCategories
const getAllDataFromTable = async (route) => {
  const response = await ncsAxios.get(`/${route}`);

  //CREATE SOME DATA MANIPULATION BEFORE PUSHING IT INTO COMPONENT
  // console.log(response.data);
  return response.data;
};

export default getAllDataFromTable;
